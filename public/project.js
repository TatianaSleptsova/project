$(document).ready(function () {
  $('.gallery').slick({
    arrows: true,
    // dots: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplay: true,

    responsive: [
      {
        breakpoint: 769,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,

        }
      },
    ]
  });
  $('.work').slick({
    centerMode: true,
    arrows: false,
    centerPadding: '10px',
    initialSlide: 1,
    slidesToShow: 3,
    infinity:true,
    autoplay: true,
    pauseOnFocus:false,
    pauseOnHover:false,
    draggable: false,
    swipe: false,
    variableWidth: true,
    autoplaySpeed: 3000,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 6
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 6
        }
      }
    ]
  });
  $('.work1').slick({
   
    arrows: false,
    centerPadding: '50px',
    initialSlide: 1,
    slidesToShow: 6,
    infinity:true,
    autoplay: true,
    pauseOnHover:false,
    centerMode: false,
    pauseOnFocus: false,
    draggable: false,
    swipe: false,
    waitForAnimate: false,
    variableWidth: true,
    autoplaySpeed: 3000,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          centerMode: false,
          centerPadding: '10px',
          slidesToShow: 6,
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: false,
          centerPadding: '10px',
          slidesToShow: 6,
        }
      }
    ]
  });
});
// $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
//   $('.work').slick('setPosition');
// })



$(function() {
  var $speed = 200; 
  var $class = 'opened';
  var $class_open = '.faq-answer';
  
  $(document).ready(function() {
     $('.faq-question').on('click', function() {
       $ul = $(this).closest('ol');
       $answer = $(this).closest('li').find($class_open);
       
       if( !$(this).closest('li').hasClass($class) ) {
       
         $ul.find('li').each(function() {
           if( $(this).hasClass($class) )
             $(this).removeClass($class).find($class_open).slideUp($speed);
         });
       }
       
       $answer
         .slideToggle($speed)
         .closest('li')
         .toggleClass($class);
     });
  });
});
